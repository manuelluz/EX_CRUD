﻿namespace CRUDListaObj
{
    partial class FormNovoAluno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TextBoxIdAluno = new System.Windows.Forms.TextBox();
            this.TextBoxPrimeiroNome = new System.Windows.Forms.TextBox();
            this.TextBoxApelido = new System.Windows.Forms.TextBox();
            this.ButtonNovoAluno = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(27, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID Aluno :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(27, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Primeiro Nome : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(27, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Apelido : ";
            // 
            // TextBoxIdAluno
            // 
            this.TextBoxIdAluno.Location = new System.Drawing.Point(156, 49);
            this.TextBoxIdAluno.Name = "TextBoxIdAluno";
            this.TextBoxIdAluno.ReadOnly = true;
            this.TextBoxIdAluno.Size = new System.Drawing.Size(149, 20);
            this.TextBoxIdAluno.TabIndex = 3;
            // 
            // TextBoxPrimeiroNome
            // 
            this.TextBoxPrimeiroNome.Location = new System.Drawing.Point(156, 91);
            this.TextBoxPrimeiroNome.Name = "TextBoxPrimeiroNome";
            this.TextBoxPrimeiroNome.Size = new System.Drawing.Size(149, 20);
            this.TextBoxPrimeiroNome.TabIndex = 4;
            // 
            // TextBoxApelido
            // 
            this.TextBoxApelido.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxApelido.Location = new System.Drawing.Point(156, 137);
            this.TextBoxApelido.Name = "TextBoxApelido";
            this.TextBoxApelido.Size = new System.Drawing.Size(149, 20);
            this.TextBoxApelido.TabIndex = 5;
            // 
            // ButtonNovoAluno
            // 
            this.ButtonNovoAluno.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonNovoAluno.BackgroundImage = global::CRUDListaObj.Properties.Resources.icon_check;
            this.ButtonNovoAluno.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ButtonNovoAluno.Location = new System.Drawing.Point(192, 197);
            this.ButtonNovoAluno.Name = "ButtonNovoAluno";
            this.ButtonNovoAluno.Size = new System.Drawing.Size(75, 45);
            this.ButtonNovoAluno.TabIndex = 6;
            this.ButtonNovoAluno.UseVisualStyleBackColor = false;
            this.ButtonNovoAluno.Click += new System.EventHandler(this.ButtonNovoAluno_Click);
            // 
            // FormNovoAluno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(328, 289);
            this.Controls.Add(this.ButtonNovoAluno);
            this.Controls.Add(this.TextBoxApelido);
            this.Controls.Add(this.TextBoxPrimeiroNome);
            this.Controls.Add(this.TextBoxIdAluno);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FormNovoAluno";
            this.Text = "Novo Aluno";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TextBoxIdAluno;
        private System.Windows.Forms.TextBox TextBoxPrimeiroNome;
        private System.Windows.Forms.TextBox TextBoxApelido;
        private System.Windows.Forms.Button ButtonNovoAluno;
    }
}
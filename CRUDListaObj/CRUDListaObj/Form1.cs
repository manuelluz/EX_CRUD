﻿namespace CRUDListaObj
{
    using Modelos;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Windows.Forms;

    public partial class Form1 : Form
    {

        private List<Aluno> ListaDeAlunos;

        private FormNovoAluno formularioNovoAluno;

        private FormApagarAluno formularioApagarAluno;

        

        public Form1()
        {
            InitializeComponent();

            ListaDeAlunos = new List<Aluno>();

            if (!CarregarAlunos())
            {
                ListaDeAlunos.Add(new Aluno { IdAluno = 16343, PrimeiroNome = "Amadeu", Apelido = "Antunes" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 16328, PrimeiroNome = "Antonio", Apelido = "Almeida" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 14990, PrimeiroNome = "Daniel", Apelido = "Veiga" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 16342, PrimeiroNome = "Joao", Apelido = "Ribeiro" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 16333, PrimeiroNome = "Luis", Apelido = "Gomes" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 12494, PrimeiroNome = "Hugo", Apelido = "Pereira" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 16334, PrimeiroNome = "Nuno", Apelido = "Serano" });
            }
                        

            ComboBoxListaAlunos.DataSource = ListaDeAlunos;

            foreach(var aluno in ListaDeAlunos)
            {
                ComboBoxNomes.Items.Add(aluno.PrimeiroNome);
            }

            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ButtonProcuarar_Click(object sender, EventArgs e)
        {
            if (ComboBoxProcurar.SelectedIndex == -1)
            {
                MessageBox.Show("Tem que escolher um critério");
                return;
            }

            int id = 0;

            if (ComboBoxProcurar.SelectedIndex == 0)
            {
                if(!int.TryParse(TextBoxProcurar.Text,out id))
                {
                    MessageBox.Show("Tem que escolher um valor numerico");
                    return;
                }

                var alunoAchado = ProcurarAluno(id);

                if (alunoAchado != null)
                {
                    MessageBox.Show(alunoAchado.NomeCompleto);
                    TextBoxIdAluno.Text= alunoAchado.IdAluno.ToString();
                    TextBoxNomeAluno.Text = alunoAchado.NomeCompleto;
                }
                else
                {
                    MessageBox.Show("Esse ID de aluno não existe!");
                }
            }
            else
            {
                var alunoAchado = ProcurarAluno(TextBoxProcurar.Text);

                if (alunoAchado != null)
                {
                    MessageBox.Show("Este aluno existe!");
                    TextBoxIdAluno.Text = alunoAchado.IdAluno.ToString();
                    TextBoxNomeAluno.Text = alunoAchado.NomeCompleto;
                }
                else
                {
                    MessageBox.Show("Este aluno não existe!");
                }
            }

        }

        private Aluno ProcurarAluno(string nome)
        {
            foreach (var aluno in ListaDeAlunos)
            {
                if (aluno.PrimeiroNome == nome)
                {
                    return aluno;
                }
            }

            return null;
        }

        private Aluno ProcurarAluno(int id)
        {
            foreach(var aluno in ListaDeAlunos)
            {
                if (aluno.IdAluno == id)
                {
                    return aluno;
                }
            }

            return null;

        }

        private void ButtonEditar_Click(object sender, EventArgs e)
        {
            TextBoxPrimeiroNome.Enabled = true;
            TextBoxApelidoAluno.Enabled = true;            
            ButtonGravar.Enabled = true;
            ButtonCancelar.Enabled = true;
        }

        private void ButtonCancelar_Click(object sender, EventArgs e)
        {
            TextBoxPrimeiroNome.Enabled = false;
            TextBoxApelidoAluno.Enabled = false;            
            ButtonGravar.Enabled = false;
            ButtonCancelar.Enabled = false;
        }

        private void ButtonGravar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxIdAluno.Text))
            {
                MessageBox.Show("Insira ID do aluno");
                return;
            }

            if (string.IsNullOrEmpty(TextBoxPrimeiroNome.Text))
            {
                MessageBox.Show("Insira o primeiro nome do aluno");
                return;
            }

            if (string.IsNullOrEmpty(TextBoxApelidoAluno.Text))
            {
                MessageBox.Show("Insira o apelido nome do aluno");
                return;
            }

            var alunoAchado = ProcurarAluno(Convert.ToInt32(TextBoxIdAluno.Text));

            if (alunoAchado != null)
            {
                alunoAchado.PrimeiroNome = TextBoxPrimeiroNome.Text;
                alunoAchado.Apelido = TextBoxApelidoAluno.Text;
                MessageBox.Show("Aluno alterado com sucesso!");

                actualizaCombos();

                TextBoxPrimeiroNome.Text = string.Empty;
                TextBoxPrimeiroNome.Enabled = false;

                TextBoxApelidoAluno.Text = string.Empty;
                TextBoxApelidoAluno.Enabled = false;

                ButtonGravar.Enabled = false;
                ButtonCancelar.Enabled = false;
            }
        }

        private void actualizaCombos()
        {
            ComboBoxListaAlunos.DataSource = null;
            ComboBoxListaAlunos.DataSource = ListaDeAlunos;

            ComboBoxNomes.Items.Clear();

            foreach (var aluno in ListaDeAlunos)
            {
                ComboBoxNomes.Items.Add(aluno.PrimeiroNome);
            }
        }

        private bool GravarAlunos()
        {
            string ficheiro = @"Alunos.txt";

            StreamWriter sw = new StreamWriter(ficheiro, false);

            try
            {
                if (!File.Exists(ficheiro))
                {
                    sw = File.CreateText(ficheiro);
                }

                foreach (var aluno in ListaDeAlunos)
                {
                    string linha = string.Format("{0};{1};{2}", aluno.IdAluno, aluno.PrimeiroNome, aluno.Apelido);
                    sw.WriteLine(linha);
                    
                }
                sw.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;              
            }

            return true;

            
        }


        private bool CarregarAlunos()
        {
            string ficheiro = @"Alunos.txt";

            StreamReader sr;
            try
            {
                if (File.Exists(ficheiro))
                {
                    sr = File.OpenText(ficheiro);

                    string linha = "";

                    while ((linha = sr.ReadLine()) != null)
                    {
                        string[] campos = new string[3];

                        campos = linha.Split(';');

                        var aluno = new Aluno
                        {
                            IdAluno = Convert.ToInt32(campos[0]),
                            PrimeiroNome = campos[1],
                            Apelido = campos[2]
                        };

                        ListaDeAlunos.Add(aluno);
                    }
                    sr.Close();
                }
                else
                {
                    return false;
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
            return true;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (GravarAlunos())
            {
                MessageBox.Show("Alunos alterados com sucesso!");
            }
        }

        private void novoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formularioNovoAluno = new FormNovoAluno(ListaDeAlunos);
            formularioNovoAluno.Show();
        }

        private void atualizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actualizaCombos();
            MessageBox.Show("Alunos atualizados com sucesso!!");
        }

        private void apagarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxIdAluno.Text))
            {
                MessageBox.Show("Insira ID a procurar");
                return;
            }

            var alunoApagado = ProcurarAluno(Convert.ToInt32(TextBoxIdAluno.Text));

            
            formularioApagarAluno = new FormApagarAluno(ListaDeAlunos,alunoApagado);

            formularioApagarAluno.Show();
        }
    }
}

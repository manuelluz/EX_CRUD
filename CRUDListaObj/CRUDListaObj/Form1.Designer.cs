﻿namespace CRUDListaObj
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ComboBoxNomes = new System.Windows.Forms.ComboBox();
            this.ComboBoxListaAlunos = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ButtonProcuarar = new System.Windows.Forms.Button();
            this.TextBoxProcurar = new System.Windows.Forms.TextBox();
            this.ComboBoxProcurar = new System.Windows.Forms.ComboBox();
            this.GroupBoxEditar = new System.Windows.Forms.GroupBox();
            this.ButtonEditar = new System.Windows.Forms.Button();
            this.TextBoxApelidoAluno = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TextBoxPrimeiroNome = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TextBoxNomeAluno = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TextBoxIdAluno = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.alunoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.apagarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.atualizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ButtonCancelar = new System.Windows.Forms.Button();
            this.ButtonGravar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.GroupBoxEditar.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ComboBoxNomes);
            this.groupBox1.Controls.Add(this.ComboBoxListaAlunos);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 42);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(346, 101);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Listagens";
            // 
            // ComboBoxNomes
            // 
            this.ComboBoxNomes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxNomes.FormattingEnabled = true;
            this.ComboBoxNomes.Location = new System.Drawing.Point(8, 65);
            this.ComboBoxNomes.Name = "ComboBoxNomes";
            this.ComboBoxNomes.Size = new System.Drawing.Size(121, 24);
            this.ComboBoxNomes.TabIndex = 1;
            // 
            // ComboBoxListaAlunos
            // 
            this.ComboBoxListaAlunos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxListaAlunos.FormattingEnabled = true;
            this.ComboBoxListaAlunos.Location = new System.Drawing.Point(8, 23);
            this.ComboBoxListaAlunos.Name = "ComboBoxListaAlunos";
            this.ComboBoxListaAlunos.Size = new System.Drawing.Size(306, 24);
            this.ComboBoxListaAlunos.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ButtonProcuarar);
            this.groupBox2.Controls.Add(this.TextBoxProcurar);
            this.groupBox2.Controls.Add(this.ComboBoxProcurar);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(364, 42);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(249, 101);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Procurar";
            // 
            // ButtonProcuarar
            // 
            this.ButtonProcuarar.Location = new System.Drawing.Point(95, 56);
            this.ButtonProcuarar.Name = "ButtonProcuarar";
            this.ButtonProcuarar.Size = new System.Drawing.Size(75, 23);
            this.ButtonProcuarar.TabIndex = 2;
            this.ButtonProcuarar.Text = "Procurar";
            this.ButtonProcuarar.UseVisualStyleBackColor = true;
            this.ButtonProcuarar.Click += new System.EventHandler(this.ButtonProcuarar_Click);
            // 
            // TextBoxProcurar
            // 
            this.TextBoxProcurar.Location = new System.Drawing.Point(135, 21);
            this.TextBoxProcurar.Name = "TextBoxProcurar";
            this.TextBoxProcurar.Size = new System.Drawing.Size(100, 22);
            this.TextBoxProcurar.TabIndex = 1;
            // 
            // ComboBoxProcurar
            // 
            this.ComboBoxProcurar.FormattingEnabled = true;
            this.ComboBoxProcurar.Items.AddRange(new object[] {
            "ID Aluno",
            "Nome"});
            this.ComboBoxProcurar.Location = new System.Drawing.Point(8, 20);
            this.ComboBoxProcurar.Name = "ComboBoxProcurar";
            this.ComboBoxProcurar.Size = new System.Drawing.Size(121, 24);
            this.ComboBoxProcurar.TabIndex = 0;
            // 
            // GroupBoxEditar
            // 
            this.GroupBoxEditar.Controls.Add(this.ButtonCancelar);
            this.GroupBoxEditar.Controls.Add(this.ButtonGravar);
            this.GroupBoxEditar.Controls.Add(this.ButtonEditar);
            this.GroupBoxEditar.Controls.Add(this.TextBoxApelidoAluno);
            this.GroupBoxEditar.Controls.Add(this.label4);
            this.GroupBoxEditar.Controls.Add(this.TextBoxPrimeiroNome);
            this.GroupBoxEditar.Controls.Add(this.label3);
            this.GroupBoxEditar.Controls.Add(this.TextBoxNomeAluno);
            this.GroupBoxEditar.Controls.Add(this.label2);
            this.GroupBoxEditar.Controls.Add(this.TextBoxIdAluno);
            this.GroupBoxEditar.Controls.Add(this.label1);
            this.GroupBoxEditar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBoxEditar.Location = new System.Drawing.Point(12, 149);
            this.GroupBoxEditar.Name = "GroupBoxEditar";
            this.GroupBoxEditar.Size = new System.Drawing.Size(601, 109);
            this.GroupBoxEditar.TabIndex = 2;
            this.GroupBoxEditar.TabStop = false;
            this.GroupBoxEditar.Text = "Editar";
            // 
            // ButtonEditar
            // 
            this.ButtonEditar.Location = new System.Drawing.Point(420, 21);
            this.ButtonEditar.Name = "ButtonEditar";
            this.ButtonEditar.Size = new System.Drawing.Size(158, 31);
            this.ButtonEditar.TabIndex = 3;
            this.ButtonEditar.Text = "Editar";
            this.ButtonEditar.UseVisualStyleBackColor = true;
            this.ButtonEditar.Click += new System.EventHandler(this.ButtonEditar_Click);
            // 
            // TextBoxApelidoAluno
            // 
            this.TextBoxApelidoAluno.Enabled = false;
            this.TextBoxApelidoAluno.Location = new System.Drawing.Point(303, 60);
            this.TextBoxApelidoAluno.Name = "TextBoxApelidoAluno";
            this.TextBoxApelidoAluno.Size = new System.Drawing.Size(100, 22);
            this.TextBoxApelidoAluno.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(230, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 16);
            this.label4.TabIndex = 6;
            this.label4.Text = "Apelido :";
            // 
            // TextBoxPrimeiroNome
            // 
            this.TextBoxPrimeiroNome.Enabled = false;
            this.TextBoxPrimeiroNome.Location = new System.Drawing.Point(92, 61);
            this.TextBoxPrimeiroNome.Name = "TextBoxPrimeiroNome";
            this.TextBoxPrimeiroNome.Size = new System.Drawing.Size(100, 22);
            this.TextBoxPrimeiroNome.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(36, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Nome :";
            // 
            // TextBoxNomeAluno
            // 
            this.TextBoxNomeAluno.Location = new System.Drawing.Point(303, 32);
            this.TextBoxNomeAluno.Name = "TextBoxNomeAluno";
            this.TextBoxNomeAluno.ReadOnly = true;
            this.TextBoxNomeAluno.Size = new System.Drawing.Size(100, 22);
            this.TextBoxNomeAluno.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(201, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nome Aluno :";
            // 
            // TextBoxIdAluno
            // 
            this.TextBoxIdAluno.Location = new System.Drawing.Point(94, 32);
            this.TextBoxIdAluno.Name = "TextBoxIdAluno";
            this.TextBoxIdAluno.ReadOnly = true;
            this.TextBoxIdAluno.Size = new System.Drawing.Size(100, 22);
            this.TextBoxIdAluno.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID Aluno :";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alunoToolStripMenuItem,
            this.atualizarToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(627, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // alunoToolStripMenuItem
            // 
            this.alunoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoToolStripMenuItem,
            this.apagarToolStripMenuItem});
            this.alunoToolStripMenuItem.Name = "alunoToolStripMenuItem";
            this.alunoToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.alunoToolStripMenuItem.Text = "Aluno";
            // 
            // novoToolStripMenuItem
            // 
            this.novoToolStripMenuItem.Name = "novoToolStripMenuItem";
            this.novoToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.novoToolStripMenuItem.Text = "Novo";
            this.novoToolStripMenuItem.Click += new System.EventHandler(this.novoToolStripMenuItem_Click);
            // 
            // apagarToolStripMenuItem
            // 
            this.apagarToolStripMenuItem.Name = "apagarToolStripMenuItem";
            this.apagarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.apagarToolStripMenuItem.Text = "Apagar";
            this.apagarToolStripMenuItem.Click += new System.EventHandler(this.apagarToolStripMenuItem_Click);
            // 
            // atualizarToolStripMenuItem
            // 
            this.atualizarToolStripMenuItem.Name = "atualizarToolStripMenuItem";
            this.atualizarToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.atualizarToolStripMenuItem.Text = "Atualizar";
            this.atualizarToolStripMenuItem.Click += new System.EventHandler(this.atualizarToolStripMenuItem_Click);
            // 
            // ButtonCancelar
            // 
            this.ButtonCancelar.Enabled = false;
            this.ButtonCancelar.Image = global::CRUDListaObj.Properties.Resources.icon_cancel;
            this.ButtonCancelar.Location = new System.Drawing.Point(503, 56);
            this.ButtonCancelar.Name = "ButtonCancelar";
            this.ButtonCancelar.Size = new System.Drawing.Size(75, 38);
            this.ButtonCancelar.TabIndex = 9;
            this.ButtonCancelar.UseVisualStyleBackColor = true;
            this.ButtonCancelar.Click += new System.EventHandler(this.ButtonCancelar_Click);
            // 
            // ButtonGravar
            // 
            this.ButtonGravar.Enabled = false;
            this.ButtonGravar.Image = global::CRUDListaObj.Properties.Resources.icon_check;
            this.ButtonGravar.Location = new System.Drawing.Point(420, 56);
            this.ButtonGravar.Name = "ButtonGravar";
            this.ButtonGravar.Size = new System.Drawing.Size(75, 38);
            this.ButtonGravar.TabIndex = 8;
            this.ButtonGravar.UseVisualStyleBackColor = true;
            this.ButtonGravar.Click += new System.EventHandler(this.ButtonGravar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(627, 327);
            this.Controls.Add(this.GroupBoxEditar);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.GroupBoxEditar.ResumeLayout(false);
            this.GroupBoxEditar.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox ComboBoxListaAlunos;
        private System.Windows.Forms.ComboBox ComboBoxNomes;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox ComboBoxProcurar;
        private System.Windows.Forms.Button ButtonProcuarar;
        private System.Windows.Forms.TextBox TextBoxProcurar;
        private System.Windows.Forms.GroupBox GroupBoxEditar;
        private System.Windows.Forms.Button ButtonCancelar;
        private System.Windows.Forms.Button ButtonGravar;
        private System.Windows.Forms.Button ButtonEditar;
        private System.Windows.Forms.TextBox TextBoxApelidoAluno;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TextBoxPrimeiroNome;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TextBoxNomeAluno;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TextBoxIdAluno;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem alunoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem apagarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem atualizarToolStripMenuItem;
    }
}


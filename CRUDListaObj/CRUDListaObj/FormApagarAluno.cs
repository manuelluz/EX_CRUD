﻿using CRUDListaObj.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUDListaObj
{
    public partial class FormApagarAluno : Form
    {

        private List<Aluno> ListaDeAlunos;

        private Aluno alunoAApagar;

        public FormApagarAluno(List<Aluno>listaAlunos,Aluno aluno)
        {
            InitializeComponent();
                        
            this.ListaDeAlunos = listaAlunos;
            alunoAApagar = aluno;

            DataGridViewAlunoApagar.Rows.Add(alunoAApagar.IdAluno, alunoAApagar.PrimeiroNome, alunoAApagar.Apelido);

            DataGridViewAlunoApagar.AllowUserToAddRows = false;
        }

        private void ButtonApagar_Click(object sender, EventArgs e)
        {
            int index = 0;

            foreach(var aluno in ListaDeAlunos)
            {
                if (alunoAApagar.IdAluno == aluno.IdAluno)
                {
                    ListaDeAlunos.RemoveAt(index);
                    break;
                }

                index++;
            }

            MessageBox.Show("Aluno Apagado com sucesso.");

            Close();

        }

        private void ButtonCancelar_Click(object sender, EventArgs e)
        {
            MessageBox.Show("O aluno nao foi apagado.");
            Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUDListaObj.Modelos
{
    public class Aluno
    {
        public int IdAluno { get; set; }

        public string PrimeiroNome { get; set; }

        public string Apelido { get; set; }

        public string NomeCompleto
        {
            get
            {
                return string.Format("{0} {1}", PrimeiroNome, Apelido);
            }
                
       }
        #region Metodos Genericos

        public override string ToString()
        {
            return string.Format("ID Aluno: {0} Nome: {1}", IdAluno, NomeCompleto);
        }

        #endregion
    }
}
